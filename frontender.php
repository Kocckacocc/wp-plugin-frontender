<?php
/*
Plugin Name: Frontender
Description: Front-end post editing with the ContentTools content editor.
Version: 1.0.3
Author: Kocckacocc
*/

if ( ! defined( 'ABSPATH' ) )
	exit;

foreach ( glob( plugin_dir_path(  __FILE__ ) . 'includes/*.php' ) as $include_file )
	require_once( $include_file );

require_once( 'includes/vendor/vendors.php' );

function frontender_init() {
	Frontender::instance();
}

frontender_init();

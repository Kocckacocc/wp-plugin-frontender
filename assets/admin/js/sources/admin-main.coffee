(($) ->
# Use $ - - - - - - - - - - - - - - - - - - - - - - - -
	$.fn.extend
		serializeObject: () ->
			o = {}
			a = @.serializeArray()
			
			$.each a, () ->
				if o[@.name] != undefined
					if !o[@.name].push
						o[@.name] = [o[@.name]]
					o[@.name].push(@.value || '')
				else
					o[@.name] = @.value || '';
			
			return o
	
	FE = {
		
		_: (s) ->
			if admin_main.localization[s]
				return admin_main.localization[s]
			else
				return s
		
		uniqueId: (length = 8) ->
			id = ''
			id += Math.random().toString(36).substr(2) while id.length < length
			id.substr 0, length
		
		after: (ms, cb) -> setTimeout cb, ms
		
	}
	
	$ ->
		$('.fe_settings_data .fe_save').on 'click', () ->
			$d = $(@).closest '.fe_settings_data'
			settings_data = $d.serializeObject()
		
			$.ajax
				url : admin_main.ajax_url,
				type: 'POST',
				data:
					action       : 'settings'
					settings_data: settings_data
				
				success: (response) ->
					if response != 'ok'
						console.log "ERR: #{response}"
					
					UIkit.notify
						message: FE._('settingssaved'),
						status : 'info',
						timeout: 3000,
						pos    : 'top-center'
	
	
	$ ->
		$('.fe_settings_data .fe_reset_settings').on 'click', () ->
			
			$.ajax
				url : admin_main.ajax_url,
				type: 'POST',
				data:
					action       : 'reset_settings'
				
				success: (response) ->
					if response != 'ok'
						console.log "ERR: #{response}"
					else
						location.reload();
						
		editor_config = $('#fe_elements_tabs_contents textarea').data 'uk-htmleditor'
		delText = $('.fe_elements .fe_del_element').first().text()
		
		removeTab = () ->
			if $('.fe_elements_tabs').find('li').length < 4
				alert FE._('You can\'t delete the last element!')
				return
				
			if !confirm FE._('Are you sure you delete this element?')
				return
			
			$('#fe_elements_tabs_contents').find('.fe_element_box.uk-active').remove()
			$('.fe_elements_tabs').find('.uk-active').remove()
			
			$('#fe_elements_tabs_contents').find('li').first().addClass 'uk-active'
			$('.fe_elements_tabs').find('li').first().addClass 'uk-active'
		
		
		$('.fe_elements .fe_del_element').on 'click', () ->
			removeTab()
		
		
		$('.fe_elements .fe_new_element').on 'click', () ->
			if $('.fe_elements_tabs').find('li').length > 13
				alert FE._('Sorry, max. 12 element allowed at the moment.')
				return
			
			$('.fe_elements .uk-active').removeClass 'uk-active'
			
			$tab = $ '<li class="uk-active"><a><i class="uk-icon-bookmark-o"></i></a></li>'
			$tab.insertBefore $(@).parent()
			
			$tabContent = $ '<li class="fe_element_box uk-active uk-animation-scale-up"></li>'
			
			$tabTextarea = $ '<textarea name="Elements_' + FE.uniqueId() + '" data-uk-htmleditor="' + editor_config + '"></textarea>'
			$tabContent.append $tabTextarea
			
			$delButton = $ '<span class="uk-button uk-button-mini fe_del_element">' + delText + '</span>'
			$delButton.on 'click', () ->
				removeTab()
			
			$delButtonWrap = $ '<div class="uk-margin-small-top"></div>'
			$delButtonWrap.append $delButton
		
			$tabContent.append $delButtonWrap
			
			$('#fe_elements_tabs_contents').append $tabContent
			
			FE.after 2000, -> $tabContent.removeClass 'uk-animation-scale-up'
		
		
		delWrapper = (e) ->
			if $('.fe_wrappers tr').length < 2
				alert FE._('You can\'t delete the last element!')
				return
				
			e.closest('tr').remove()
		
		$('.fe_styles .fe_del_wrapper').on 'click', () ->
			delWrapper $(@)
			
		$('.fe_styles .fe_new_wrapper').on 'click', () ->
			
			wrapperClone = $(@).data 'wrapper_clone'
			
			uniqueId = FE.uniqueId()
			wrapperClone = $ wrapperClone.replace /random/g, uniqueId
			
			wrapperClone.find('.fe_del_wrapper').on 'click', () ->
				delWrapper $(@)
			
			$('.fe_wrappers').append wrapperClone
		
		
		delClasser = (e) ->
			if $('.fe_classers tr').length < 2
				alert FE._('You can\'t delete the last element!')
				return
			
			e.closest('tr').remove()
		
		$('.fe_styles .fe_del_classer').on 'click', () ->
			delClasser $(@)
		
		$('.fe_styles .fe_new_classer').on 'click', () ->
			
			classerClone = $(@).data 'classer_clone'
			
			uniqueId = FE.uniqueId()
			classerClone = $ classerClone.replace /random/g, uniqueId
			
			classerClone.find('.fe_del_classer').on 'click', () ->
				delClasser $(@)
			
			$('.fe_classers').append classerClone
			
				
# JQuery placeholder - - - - - - - - - - - - - - - - - - - - - - - -
) jQuery
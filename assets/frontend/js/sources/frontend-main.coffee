(($) ->
# Use $ - - - - - - - - - - - - - - - - - - - - - - - -
	FE = {
		
		_: (s) ->
			if frontend_main.localization[s]
				return frontend_main.localization[s]
			else
				return s
		
		checkLock : () ->
			
			postLockDialog = $ '#post-lock-dialog'
			
			if postLockDialog.length > 0
				
				$editButton = $ '.ct-ignition__button--edit'
				
				$editButtonClone = $editButton.clone(true).off()
				
				$editButton.replaceWith $editButtonClone
				
				$editButtonClone.on 'click', (e) ->
					
					postLockDialog.removeClass 'fe_hidden'
					postLockDialog.find('.fe_cancel_edit').on 'click', ->
						postLockDialog.addClass 'fe_hidden'
					
					postLockDialog.find('.fe_relase_lock').on 'click', ->
						postLockDialog.remove()
						$editButtonClone.replaceWith $editButton
						
						$editButton.trigger('click');
	
		getlock : () ->
			locked = []
			
			$.each editor.domRegions(), (i, c) =>
				data = $(c).data 'name'
				
				[name, post_id] = data.split '-'
				locked.push post_id
			
			locked
			
		after : (ms, cb) -> setTimeout cb, ms
			
		every : (ms, cb) -> setInterval cb, ms
	}
		
	editor = ContentTools.EditorApp.get();
	
	editor.init '[data-editable]', 'data-name'
	
	addHelpers = false
	
	editor.addEventListener 'start', () ->
		
		if addHelpers
			$('.ce-signed').after '&nbsp;<span class="ce-helper">...</span>'
			$('.ce-signed').before '<span class="ce-helper">...</span>&nbsp;'
		
		locked = FE.getlock()
	
		if locked.length != 0
			$.ajax
				url : frontend_main.ajax_url,
				type: 'POST',
				data:
					action: 'setlock'
					locked: locked
					lock  : true
	
	editor.addEventListener 'stop', (ev) ->
		
		if addHelpers
			FE.after 0, ->
				$('.ce-helper').remove()
				
				$.each editor.domRegions(), (i, c) =>
					$(c).html($(c).html().replace(/&nbsp;/g, ''))
		
		if !ev.detail().save
			$.ajax
				url : frontend_main.ajax_url,
				type: 'POST',
				data:
					action: 'setlock'
					locked: FE.getlock()
	
	editor.addEventListener 'saved', (ev) ->
		regions = ev.detail().regions
		changed = {}
		
		$.each regions, (i, c) =>
			[name, post_id] = i.split '-'
			changed[post_id] = $.trim c
		
		editor.busy(true)
		
		$.ajax
			url : frontend_main.ajax_url,
			type: 'POST',
			data:
				action : 'update'
				changed: changed
				locked : FE.getlock()
			
			success: (data) ->
				if data != 'ok'
					console.log "ERR: #{data}"
				else
					if Object.keys(regions).length != 0
						new ContentTools.FlashUI data
		
		editor.busy(false);
	
	$ ->
		
		FE.after 100, -> FE.checkLock()
		
# JQuery placeholder - - - - - - - - - - - - - - - - - - - - - - - -
) jQuery
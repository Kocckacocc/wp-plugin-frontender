# console.log frontend_content_tools_config

# tools

ContentTools.DEFAULT_TOOLS = [
	[
		'bold'
		'italic'
		'link'
		'align-left'
		'align-center'
		'align-right'
		'elemental'
		'wrapper'
		'clearformat'
	]
	[
		'heading'
		'subheading'
		'paragraph'
		'unordered-list'
		'ordered-list'
		'table'
		'indent'
		'unindent'
		'line-break'
	]
	[
		'wpimage'
		'video'
		'preformatted'
	]
	[
		'undo'
		'redo'
		'remove'
	]
]

# block classes

if typeof frontend_content_tools_config.block_classes == 'object'
	if Object.keys(frontend_content_tools_config.block_classes).length > 0
		for k,v of frontend_content_tools_config.block_classes
			ContentTools.StylePalette.add([
				new ContentTools.Style(v.name, v.class)
			])

# translate

console.log frontend_content_tools_config.localization

ContentEdit.addTranslations frontend_content_tools_config.locale, frontend_content_tools_config.localization;
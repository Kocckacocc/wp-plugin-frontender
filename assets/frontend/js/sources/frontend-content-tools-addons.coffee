(($) ->
# Use $ - - - - - - - - - - - - - - - - - - - - - - - - -
	class ContentTools.Tools.Clearformat extends ContentTools.Tool
		
		# Clear all formatting from selection
		
		ContentTools.ToolShelf.stow(@, 'clearformat')
		
		@label = 'Clear all formatting'
		@icon = 'clearformat'
		
		@canApply: (element, selection) ->
			unless element.content
				return false
			
			return selection and not selection.isCollapsed()
		
		@apply: (element, selection, callback) ->
			[from, to] = selection.get()
			
			element.content = element.content.unformat(
				from,
				to
			)
			
			element.content.optimize()
			element.updateInnerHTML()
			element.taint()
			
			element.restoreState()
			
			callback(true)
	
	
	class ContentTools.Tools.Wrapper extends ContentTools.Tool
		
		# Wraps the current selection.
		
		ContentTools.ToolShelf.stow(@, 'wrapper')
		
		@label = 'Wrapper'
		@icon = 'wrapper'
		
		@canApply: (element, selection) ->
			unless element.content
				return false
			
			return selection and not selection.isCollapsed()
		
		@isApplied: (element, selection) ->
			if element.content is undefined or not element.content.length()
				return false
			
			[from, to] = selection.get()
			if from == to
				to += 1
			
			return element.content.slice(from, to).hasTags('span', true)
		
		@apply: (element, selection, callback) ->
			element.storeState()
			
			app = ContentTools.EditorApp.get()
			modal = new ContentTools.ModalUI()
			dialog = new ContentTools.WrapperDialog()
			
			dialog.addEventListener 'cancel', () =>
				modal.hide()
				dialog.hide()
				element.restoreState()
				callback(false)
			
			dialog.addEventListener 'save', (ev) =>
				detail = ev.detail()
				
				[from, to] = selection.get()
				
				attributes =
					class: 'ce-signed ce-wrapped-signed ' + detail.classes
					style: detail.style
				
				
				if @isApplied(element, selection)
					element.content = element.content.unformat(
						from,
						to
					)

				element.content = element.content.format(
					from,
					to,
					new HTMLString.Tag('span', attributes)
				)
				
				element.content.optimize()
				element.updateInnerHTML()
				element.taint()
				
				element.restoreState()
				
				modal.hide()
				dialog.hide()
			
			app.attach(modal)
			app.attach(dialog)
			
			modal.show()
			dialog.show()
	
	class ContentTools.WrapperDialog extends ContentTools.DialogUI
		
		# Dialog for the wrapper
		
		constructor: ()->
			super('Wrap the selection')
		
		mount: () ->
			super()
			
			$domControls = $ @_domControls
			$domView = $ @_domView
			
			$domElements = $ '<div></div>'
			$domElements.addClass 'ct-elemental-dialog__elements'
			$domView.append $domElements
			
			console.log frontend_content_tools_addons.wrappers
			
			$.each frontend_content_tools_addons.wrappers, (i, c) =>
				$content = $ '<span style="' + c.style + '" class="' + c.classes + '">' + ContentEdit._('Selection') + '</span>'
				
				$element = $ '<div></div>'
				$element.addClass 'element'
				$element.data('toSend', c)
				$element.append $content
				
				$element.on 'click', () ->
					$domElements.find('.element').removeClass('choosen')
					$(this).addClass 'choosen'
					
					$domButton.removeClass 'ct-control--muted'
				
				$domElements.append $element
			
			$domButton = $ '<div>' + ContentEdit._('Wrap it') + '</div>'
			$domButton.addClass 'ct-control ct-control--text ct-control--insert ct-control--muted'
			$domControls.append $domButton
			
			$domButton.on 'click', () =>
				if !$domButton.hasClass('ct-control--muted')
					
					$choosen = $domElements.find '.choosen'
					
					@dispatchEvent(@createEvent('save', $choosen.data('toSend')))
			
			@_addDOMEventListeners()
	
	
	class ContentTools.Tools.WpImage extends ContentTools.Tool
		
		# Insert an image from WP media library.
		
		ContentTools.ToolShelf.stow(@, 'wpimage')
		
		@label = 'Image'
		@icon = 'image'
		
		@canApply: (element, selection) ->
			return not element.isFixed()
		
		@apply: (element, selection, callback) ->
		
		
			mediaUploader = wp.media
				title              : ContentEdit._('Insert image')
				multiple           : false
				allowLocalEdits    : true
				displaySettings    : true
				displayUserSettings: true
				frame              : 'post'
				state              : 'insert'
			
			mediaUploader.open()
			
			mediaUploader.on 'insert', () =>
				
				size = $(mediaUploader.el).find('.size :selected').val()
				
				attachment = mediaUploader.state().get('selection').first().toJSON()
				
				attachment = attachment.sizes[size]
				
				imageAttrs =
					src   : attachment.url
					width : attachment.width
					height: attachment.height
				
				image = new ContentEdit.Image(imageAttrs)
				
				[node, index] = @_insertAt(element)
				node.parent().attach(image, index)
				image.focus()
				
				callback(true)
	
	class ContentTools.Tools.Elemental extends ContentTools.Tool
		
		# Inserts specific element
		
		ContentTools.ToolShelf.stow(@, 'elemental')
		
		@label = 'Custom element'
		@icon = 'elemental'
		
		@canApply: (element, selection) ->
			return element.content
				
		
		@apply: (element, selection, callback) ->
			if element.storeState
				element.storeState()
			
			app = ContentTools.EditorApp.get()
			modal = new ContentTools.ModalUI()
			dialog = new ContentTools.ElementalDialog()
			
			dialog.addEventListener 'cancel', () =>
				modal.hide()
				dialog.hide()
				
				if element.restoreState
					element.restoreState()
				
				callback(false)
			
			dialog.addEventListener 'save', (ev) =>
				detail = ev.detail()
				htmlToInsert = detail.htmlToInsert
				
				cursor = selection.get()[0] + 1
				
				tip = element.content.substring(0, selection.get()[0])
				tail = element.content.substring(selection.get()[1])
				
				insert = new HTMLString.String('&nbsp;' + htmlToInsert + '&nbsp;', element.content.preserveWhitespace())
				
				element.content = tip.concat(insert, tail)
				
				element.updateInnerHTML()
				element.taint()
				
				selection.set(cursor, cursor)
				element.selection(selection)
				
				modal.hide()
				dialog.hide()
				
				callback(true)
			
			app.attach(modal)
			app.attach(dialog)
			
			modal.show()
			dialog.show()
	
	class ContentTools.ElementalDialog extends ContentTools.DialogUI
		
		# Dialog for the elemental
		
		constructor: ()->
			super('Insert custom element')
		
		mount: () ->
			super()
			
			$domControls = $ @_domControls
			$domView = $ @_domView
			
			$domElements = $ '<div class="ct-elemental-dialog__elements"></div>'
			$domView.append $domElements
			
			$elementConfig =
				'<div class="ct-elemental-dialog__config ct-disabled">' +
					'<p>' +
					'<label class="e_w_sm">' + ContentEdit._('Link Text') + '</label><input disabled class="e_ltext" type="text" />' +
					'<label class="e_w_md">' + ContentEdit._('Classes') + '</label><input disabled class="e_classes" type="text" />' +
					'<p>' +
					'<label class="e_w_sm">' + ContentEdit._('Link href') + '</label><input disabled class="e_lhref" type="text" />' +
					'<label class="e_w_md">' + ContentEdit._('New window?') + '</label><input disabled class="e_target" type="checkbox" />' +
					'</p>' +
					'</div>'
			
			$elementConfig = $ $elementConfig
			$domView.append $elementConfig
			
			$.each frontend_content_tools_addons.elements, (i, c) =>
				$content = $ c
				$content.addClass 'ce-signed'
				
				$element = $ '<div></div>'
				$element.addClass 'element'
				$element.append $content
				
				$element.on 'click', (e) ->
					e.preventDefault()
					
					$domElements.find('.element').removeClass('choosen')
					
					$(@).addClass 'choosen'
					$inner = $(@).find('.ce-signed')
					
					if $inner.is('a')
						$elementConfig.find('input').removeAttr('disabled')
						$elementConfig.removeClass('ct-disabled')
						
						classes = $inner.attr('class').replace('ce-signed', '').trim()
						
						$elementConfig.find('.e_ltext').val($inner.html())
						$elementConfig.find('.e_classes').val(classes)
						
					else
						$elementConfig.find('input').attr('disabled', 'disabled')
						$elementConfig.addClass('ct-disabled')
					
					$domButton.removeClass 'ct-control--muted'
				
				$domElements.append $element
			
			$domButton = $ '<div>' + ContentEdit._('Insert') + '</div>'
			$domButton.addClass 'ct-element-insert ct-control ct-control--text ct-control--insert ct-control--muted'
			$domControls.append $domButton
		
			$domButton.on 'click', () =>
				if !$domButton.hasClass('ct-control--muted')
					
					$toInsert = $domElements.find('.choosen .ce-signed')
					if $toInsert.is('a')
						
						content = $elementConfig.find('.e_ltext').val()
						if content == '' then content = '.'
						
						$toInsert
							.html(content)
							.attr('class', $elementConfig.find('.e_classes').val() + ' ce-signed ce-inline-signed')
							.attr('href', $elementConfig.find('.e_lhref').val())
						
						if $elementConfig.find('.e_target').is(':checked')
							$toInsert.attr('target', '_blank')
				
					else
						
						$toInsert.addClass 'ce-inline-signed'
							
					htmlString = $toInsert.parent().html()
					
					@dispatchEvent(@createEvent('save', {'htmlToInsert': htmlString}))
			
			@_addDOMEventListeners()
	
	$ ->


# JQuery placeholder - - - - - - - - - - - - - - - - - - - - - - - - -
) jQuery
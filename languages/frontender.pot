#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Frontender\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-10-01 18:16+0200\n"
"PO-Revision-Date: 2016-09-23 19:46+0200\n"
"Last-Translator: \n"
"Language-Team: Kocckacocc <info@professoftware.hu>\n"
"Language: hu_HU\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;"
"_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;"
"esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;"
"esc_html_x:1,2c\n"
"X-Generator: Poedit 1.8.2\n"
"X-Poedit-SourceCharset: UTF-8\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: assets\n"
"X-Poedit-SearchPathExcluded-1: languages\n"

#: includes/class-frontender-config.php:24
msgid "For Beginners"
msgstr ""

#: includes/class-frontender-config.php:27
msgid "Get Started"
msgstr ""

#: includes/class-frontender-config.php:28
#: includes/class-frontender-config.php:36
#: includes/class-frontender-config.php:40
#: includes/class-frontender-config.php:45
msgid "Stay tuned... Help coming soon!"
msgstr ""

#: includes/class-frontender-config.php:32
msgid "For Developers"
msgstr ""

#: includes/class-frontender-config.php:35
msgid "Customization"
msgstr ""

#: includes/class-frontender-config.php:39
msgid "Troubleshooting"
msgstr ""

#: includes/class-frontender-config.php:44
msgid "FAQ"
msgstr ""

#: includes/class-frontender-config.php:52
msgid "Administrator"
msgstr ""

#: includes/class-frontender-config.php:56
msgid "Editor"
msgstr ""

#: includes/class-frontender-config.php:60
msgid "Author"
msgstr ""

#: includes/class-frontender-config.php:64
msgid "Contributor"
msgstr ""

#: includes/class-frontender-config.php:67
msgid "Subscriber"
msgstr ""

#: includes/class-frontender-config.php:89
msgid "Clearer"
msgstr ""

#: includes/class-frontender-config.php:93
#: includes/class-frontender-config.php:97
#: includes/class-frontender-config.php:101
msgid "Width"
msgstr ""

#: includes/class-frontender-config.php:105
msgid "Auto width"
msgstr ""

#: includes/class-frontender-config.php:110
msgid "Pull Right"
msgstr ""

#: includes/class-frontender-config.php:115
msgid "Alert"
msgstr ""

#: includes/class-frontender-config.php:119
msgid "Green"
msgstr ""

#: includes/class-frontender-config.php:119
#: includes/class-frontender-config.php:123
msgid "alert modifier"
msgstr ""

#: includes/class-frontender-config.php:123
msgid "Red"
msgstr ""

#: includes/class-frontender-config.php:128
#: includes/class-frontender-config.php:133
#: includes/class-frontender-config.php:138
#: includes/class-frontender-config.php:143
msgid "Spacing"
msgstr ""

#: includes/class-frontender-config.php:128
msgid "top"
msgstr ""

#: includes/class-frontender-config.php:133
msgid "bottom"
msgstr ""

#: includes/class-frontender-config.php:138
msgid "left"
msgstr ""

#: includes/class-frontender-config.php:143
msgid "right"
msgstr ""

#: includes/class-frontender-config.php:149
msgid "Border bottom"
msgstr ""

#: includes/class-frontender-config.php:180
#: includes/class-frontender-config.php:181
#: includes/class-frontender-config.php:182
msgid "Large Button"
msgstr ""

#: includes/class-frontender-config.php:184
#: includes/class-frontender-config.php:185
#: includes/class-frontender-config.php:186
msgid "Smaller button"
msgstr ""

#: includes/class-frontender-frontend.php:173
msgid "This content is currently locked."
msgstr ""

#: includes/class-frontender-frontend.php:173
#, php-format
msgid "If you take over, %s will be blocked from continuing to edit."
msgstr ""

#: includes/class-frontender-frontend.php:176
msgid "Go back"
msgstr ""

#: includes/class-frontender-frontend.php:177
msgid "Take over"
msgstr ""

#: includes/class-frontender-settings-page.php:23
msgid "Settins"
msgstr ""

#: includes/class-frontender-settings-page.php:224
msgid "Frontender Settings"
msgstr ""

#: includes/class-frontender-settings-page.php:255
msgid "Permissions"
msgstr ""

#: includes/class-frontender-settings-page.php:256
msgid "Elements"
msgstr ""

#: includes/class-frontender-settings-page.php:257
#: includes/config/jsvars/frontend/frontend-content-tools-config.php:57
msgid "Styles"
msgstr ""

#: includes/class-frontender-settings-page.php:303
msgid "Save settings"
msgstr ""

#: includes/class-frontender-settings-page.php:304
msgid "Reset settings"
msgstr ""

#: includes/class-frontender-settings-page.php:317
msgid "Edit Pages"
msgstr ""

#: includes/class-frontender-settings-page.php:318
msgid "Edit Posts"
msgstr ""

#: includes/class-frontender-settings-page.php:319
msgid "Relase Lock"
msgstr ""

#: includes/class-frontender-settings-page.php:327
msgid "Role"
msgstr ""

#: includes/class-frontender-settings-page.php:383
msgid "+ New"
msgstr ""

#: includes/class-frontender-settings-page.php:392
msgid "Preview"
msgstr ""

#: includes/class-frontender-settings-page.php:408
msgid "Delete element"
msgstr ""

#: includes/class-frontender-settings-page.php:427
msgid "Inline"
msgstr ""

#: includes/class-frontender-settings-page.php:428
msgid "Block"
msgstr ""

#: includes/class-frontender-settings-page.php:437
msgid "Class(es)"
msgstr ""

#: includes/class-frontender-settings-page.php:438
msgid "Style tag"
msgstr ""

#: includes/class-frontender-settings-page.php:474
msgid "Add wrapper"
msgstr ""

#: includes/class-frontender-settings-page.php:484
#: includes/config/jsvars/frontend/frontend-content-tools-config.php:52
msgid "Name"
msgstr ""

#: includes/class-frontender-settings-page.php:485
msgid "Class"
msgstr ""

#: includes/class-frontender-settings-page.php:521
msgid "Add block class"
msgstr ""

#: includes/class-frontender-settings-page.php:533
msgid "Coming soon!..."
msgstr ""

#: includes/class-frontender.php:73
#, php-format
msgid "Frontender need WP version %s or higher."
msgstr ""

#: includes/config/jsvars/admin/admin-main.php:7
msgid "Settings saved!"
msgstr ""

#: includes/config/jsvars/admin/admin-main.php:8
msgid "You can't delete the last element!"
msgstr ""

#: includes/config/jsvars/admin/admin-main.php:9
msgid "Sorry, max. 12 element allowed at the moment."
msgstr ""

#: includes/config/jsvars/admin/admin-main.php:10
msgid "Are you sure you delete this element?"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:7
msgid "Bold"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:8
msgid "Italic"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:9
msgid "Link"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:10
msgid "Enter a link"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:11
msgid "Align left"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:12
msgid "Align center"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:13
msgid "Align right"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:14
msgid "Custom element"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:15
msgid "Insert custom element"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:16
msgid "Wrapper"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:17
msgid "Wrap the selection"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:18
msgid "Wrap it"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:19
msgid "Clear all formatting"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:20
msgid "Heading"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:21
msgid "Subheading"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:22
msgid "Paragraph"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:23
msgid "Insert table"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:24
msgid "Table"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:25
msgid "Update table"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:26
msgid "Table body (columns)"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:27
msgid "Table foot"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:28
msgid "Table head"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:29
msgid "Table row"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:30
msgid "List"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:31
msgid "List item"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:32
msgid "Bullet list"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:33
msgid "Numbers list"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:34
msgid "Indent"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:35
msgid "Unindent"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:36
msgid "Line break"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:37
msgid "Image"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:38
msgid "Insert image"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:39
msgid "Upload"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:40
msgid "Rotate"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:41
msgid "Crop marks"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:42
msgid "Video"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:43
msgid "Insert video"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:44
msgid "Paste YouTube or Vimeo URL"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:45
msgid "Preformatted"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:46
msgid "Code"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:47
msgid "Apply"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:48
msgid "Attributes"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:49
msgid "Cancel"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:50
msgid "Clear"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:51
msgid "Insert"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:53
msgid "No styles available for this tag"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:54
msgid "Properties"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:55
msgid "Redo"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:56
msgid "Remove"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:58
msgid "Text"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:59
msgid "Undo"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:60
msgid "Value"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:61
msgid "Link Text"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:62
msgid "Classes"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:63
msgid "Link href"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:64
msgid "New window?"
msgstr ""

#: includes/config/jsvars/frontend/frontend-content-tools-config.php:68
msgid "Your changes have not been saved, do you really want to lose them?"
msgstr ""

��    v      �  �   |      �	     �	     �	     
     
     !
     '
  
   4
     ?
     K
  %   Q
  
   w
     �
  
   �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
     �
     �
     �
             
        '     6     D  
   S  
   ^     i     p     y     �     �     �     �  (   �     �     �     �     �  =        D     J     Q     X     _     u     �     �     �     �  
   �     �  	   �  	   �     �  	   �     �     �      �       	   "     ,     G     S     `  
   h  
   s     ~     �     �     �     �     �     �     �     �     �     �  -   �             	   @     J  
   Q  
   \     g     m  
   �  
   �  	   �  	   �     �  !   �     �     �     �     �     �                              2  "   :  B   ]     �     �     �     �     �  �  �     �     �  
   �     �     �     �          "     4  &   @     g     u     }     �     �     �     �     �     �     �  
   �     �     �          &  
   +     6     F     R     ^     l  	   {     �     �     �     �     �  
   �     �     �  7   �     3     D     K     Q  7   V     �     �     �  
   �  "   �     �     �     
       	   #  
   -     8     D     Q     ]  	   c     m     r  /   �     �  	   �     �     �     �  
                   (     .     4     E     N     k     r     ~     �     �     �  .   �     �              	   (     2     9  
   F  "   Q     t     �     �  	   �     �  $   �     �     �     �          /     ;     C     J     V     _     t  !   �  =   �     �                         5   *   j   s   	   Z       A      O          2      a   p      r   :   
       <          J       d   0       C       Q   P      D   (          ^   F       E   "   &           u   =   S   V   i          e       #   M      $   N   ,                               >   4   )   v       W           6   b       l      I         q         -   c   !       m   .   B   h   t       R   K   _   f           9          3      ;   /      `                  g   G   ?   U       X   k                                   o   '   T   @   7   +       L   [           \   %   ]   1   8          n   H       Y       + New Add block class Add wrapper Administrator Alert Align center Align left Align right Apply Are you sure you delete this element? Attributes Author Auto width Block Bold Border bottom Bullet list Cancel Class Class(es) Classes Clear Clear all formatting Clearer Code Coming soon!... Contributor Crop marks Custom element Customization Delete element Edit Pages Edit Posts Editor Elements Enter a link FAQ For Beginners For Developers Frontender Settings Frontender need WP version %s or higher. Get Started Go back Green Heading If you take over, %s will be blocked from continuing to edit. Image Indent Inline Insert Insert custom element Insert image Insert table Insert video Italic Large Button Line break Link Link Text Link href List List item Name New window? No styles available for this tag Numbers list Paragraph Paste YouTube or Vimeo URL Permissions Preformatted Preview Properties Pull Right Red Redo Relase Lock Remove Reset settings Role Rotate Save settings Settings saved! Settins Smaller button Sorry, max. 12 element allowed at the moment. Spacing Stay tuned... Help coming soon! Style tag Styles Subheading Subscriber Table Table body (columns) Table foot Table head Table row Take over Text This content is currently locked. Troubleshooting Undo Unindent Update table Upload Value Video Width Wrap it Wrap the selection Wrapper You can't delete the last element! Your changes have not been saved, do you really want to lose them? alert modifier bottom left right top Project-Id-Version: Frontender
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2016-10-01 18:14+0200
PO-Revision-Date: 2016-10-01 18:15+0200
Last-Translator: 
Language-Team: Kocckacocc <info@professoftware.hu>
Language: hu_HU
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2;_ex:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Generator: Poedit 1.8.2
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-Basepath: ..
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: assets
X-Poedit-SearchPathExcluded-1: languages
 + Új Új Blokk osztály Beágyazó Adminisztrátor Figyelmeztődoboz Középre igazítás Balra igazítás Jobbra igazítás Alkalmazás Biztos hogy töröljük ezt az elemet? Attribútumok Szerző Automatikus szélesség Blokk Félkövér Alsó keretszegély Felsorolás Mégsem Osztály Osztály(ok) Osztályok Kirüítés Formázás törlése Lebegőoszlop záró Kód Hamarosan! Közreműködő Vágójelek Egyedi elem Testreszabás Elem törlése Oldalakon Bejegyzéseken Szerkesztő Elemek Hivatkozás beszúrása GY.I.K. Kezdőknek Fejlesztőknek Frontender beállítások A Frontendernek legalább %s verziójú WP szükséges! Kezdő lépések Vissza Zöld Cím Ha átvesszük, %s  nem fog tudni tovább szerkeszteni. Kép Behúzás növelése Beágyazott Beszúrás Előre definiált elem beszúrása Kép beszúrása Táblázat beszúrása Videó beszúrása Dőlt Nagy Gomb Sortörés Hivatkozás Link szöveg Hivatkozás Lista Listaelem Név Új ablakban? Ehhez az elemhez nincsenek elérhető stílusok Számozott lista Bekezdés YouTube vagy Vimeo URL Jogosultságok Előformázott Előnézet Tulajdonságok Jobbra húzva Piros Újra Zárolás oldás Törlés Beállítások alaphelyzetbe Szerep Elforgatás Beállítások mentése Beállítások Mentve! Beállítások Kisebb Gomb Sajnáljuk, max. 12 elem lehetséges jelenleg. Térköz Türelem, segítség hamarosan! Stílus Stílusok Alcím Feliratkozó Táblázat Táblázattörzs (oszlopok száma) Táblázat-lábléc Táblázat-fejléc Táblázat sor Átvétel Szöveg Ez a tartalom jelenleg zárolva van. Hibaelhárítás Visszavonás Behúzás csökkentése Táblázat frissítése Feltöltés Érték Videó Szélesség Hozzáad Stílus hozzáadása Kijelölés Stílusa Nem törölhető az utolsó elem! A változtatások nincsenek elmentve. Biztosan elveted őket? Figyelmeztődoboz Színező alsó bal right felső 
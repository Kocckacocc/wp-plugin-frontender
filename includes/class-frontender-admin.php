<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

class Frontender_Admin {

	public $config = array();

	public function __construct( $config ) {

		$this->config = $config;

		new Frontender_Settings_Page( $config );

		// Some hack

		add_filter( 'tiny_mce_before_init', function ( $settings ) {
			$settings['forced_root_block'] = 'div';
			$settings['block_formats']     = '-----=div;Paragraph=p;H1=h1;H2=h2;H3=h3;H4=h4';

			return $settings;
		} );

		// Ajax Actions

		add_action( 'wp_ajax_setlock', array( $this, 'ajax_setlock' ) );

		add_action( 'wp_ajax_getlock', array( $this, 'ajax_getlock' ) );

		add_action( 'wp_ajax_update', array( $this, 'ajax_update' ) );

		add_action( 'wp_ajax_settings', array( $this, 'ajax_settings' ) );

		add_action( 'wp_ajax_reset_settings', array( $this, 'ajax_reset_settings' ) );

	}

	public function ajax_setlock() {
		$this->_set_lock( $_REQUEST['locked'], isset( $_REQUEST['lock'] ) );
		wp_die();
	}

	private function _set_lock( $posts, $lock = false ) {
		if ( ! count( $posts ) || count( $posts ) == 0 )
			return;

		foreach ( $posts as $post )
			if ( $lock )
				wp_set_post_lock( $post );
			else
				$this->_relase_lock( $post );

	}

	private function _relase_lock( $post_id ) {
		if ( ! $post = get_post( $post_id ) )
			return false;

		delete_post_meta( $post->ID, '_edit_lock' );

		return true;
	}

	public function ajax_getlock() {
		echo $this->_check_lock( $_REQUEST['locked'] );
		wp_die();
	}

	private function _check_lock( $posts ) {
		if ( ! count( $posts ) || count( $posts ) == 0 )
			return false;

		$return = false;

		foreach ( $posts as $post_id ) {

			if ( ! $post = get_post( $post_id ) )
				continue;

			if ( ! $lock = get_post_meta( $post->ID, '_edit_lock', true ) )
				continue;

			$lock = explode( ':', $lock );
			$time = $lock[0];
			$user = isset( $lock[1] ) ? $lock[1] : get_post_meta( $post->ID, '_edit_last', true );

			$userdata = get_userdata( $user );
			$return   = $userdata->user_login;

		}

		return $return ? $return : 'false';

	}

	public function ajax_update() {
		$this->_set_lock( $_REQUEST['locked'] );

		$response = 'ok';

		if ( isset( $_REQUEST['changed'] ) ) {

			foreach ( $_REQUEST['changed'] as $post_id => $post_content ) {

				$post = get_post( $post_id );

				$post->post_content = str_replace( array( '\'', '\"' ), '"', $post_content );

				$post_id = wp_update_post( $post );

				if ( is_wp_error( $post_id ) ) {
					$response = "ERRORS!\n";
					$errors   = $post_id->get_error_messages();
					foreach ( $errors as $error ) {
						$response .= $error . "\n";
					}
				}

			}

		}

		echo $response;

		wp_die();
	}

	public function ajax_settings() {

		$settings_data = array();

		foreach ( $_REQUEST['settings_data'] as $data_key => $data_val ) {

			$data_keys = explode( '_', $data_key );

			switch ( $data_keys[0] ) {

				case 'Permissions':
					$settings_data['roles'][ $data_keys[1] ]['can'][] = $data_keys[2];
					break;

				case 'Elements':
					$settings_data['jsvars']['elements'][] = $data_val;
					break;

				case 'Wrapper':
					$settings_data['jsvars']['wrappers'][ $data_keys[1] ][ $data_keys[2] ] = $data_val;
					break;

				case 'Blockclass':
					$settings_data['jsvars']['block_classes'][ $data_keys[1] ][ $data_keys[2] ] = $data_val;
					break;

			}

		}

		foreach ( $settings_data['jsvars']['wrappers'] as $data_key => $data_val ) {
			if ( $data_val['classes'] == '' && $data_val['style'] == '' ) {
				unset( $settings_data['jsvars']['wrappers'][ $data_key ] );
			}
		}

		foreach ( $settings_data['jsvars']['block_classes'] as $data_key => $data_val ) {
			if ( $data_val['name'] == '' || $data_val['class'] == '' ) {
				unset( $settings_data['jsvars']['block_classes'][ $data_key ] );
			}
		}

		$settings_data['jsvars']['wrappers'] = array_values( $settings_data['jsvars']['wrappers'] );

		foreach ( $settings_data['roles'] as $role_key => $role_val ) {
			$settings_data['roles'][ $role_key ]['can'] = implode( ',', $role_val['can'] );
		}

		update_option( $this->config['option_prefix'] . '_settings', $settings_data );

		echo 'ok';

		wp_die();
	}

	public function ajax_reset_settings() {

		delete_option( $this->config['option_prefix'] . '_settings' );

		echo 'ok';

		wp_die();
	}

}
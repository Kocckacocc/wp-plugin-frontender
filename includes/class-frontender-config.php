<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

class Frontender_Config {

	public $config = array();

	public function __construct() {

		add_filter( 'show_admin_bar', '__return_false' );

		$this->config = array(
			'version'       => '1.0.3',
			'wp_version'    => '4.0',
			'shortname'     => 'Frontender',
			'option_prefix' => 'frontender',

			'admin' => array(
				'sticky' => array(
					array(
						'class' => 'uk-nav-header',
						'title' => __( 'For Beginners', FE_TD )
					),
					array(
						'title' => __( 'Get Started', FE_TD ),
						'modal' => __('Stay tuned... Help coming soon!', FE_TD ),
					),
					array(
						'class' => 'uk-nav-header',
						'title' => __( 'For Developers', FE_TD )
					),
					array(
						'title' => __( 'Customization', FE_TD ),
						'modal' => __('Stay tuned... Help coming soon!', FE_TD ),
					),
					array(
						'title' => __( 'Troubleshooting', FE_TD ),
						'modal' => __('Stay tuned... Help coming soon!', FE_TD ),
					),
					array( 'class' => 'uk-nav-divider' ),
					array(
						'title' => '<i class="uk-icon-info-circle uk-margin-small-right"></i>' . __( 'FAQ', FE_TD ),
						'modal' => __('Stay tuned... Help coming soon!', FE_TD ),
					),
				)
			),

			'roles' => array(
				'administrator' => array(
					'title' => __( 'Administrator', FE_TD ),
					'can'   => 'pages,posts,lock',
				),
				'editor'        => array(
					'title' => __( 'Editor', FE_TD ),
					'can'   => 'pages,posts,lock',
				),
				'author'        => array(
					'title' => __( 'Author', FE_TD ),
					'can'   => 'pages,posts',
				),
				'contributor'   => array(
					'title' => __( 'Contributor', FE_TD )
				),
				'subscriber'    => array(
					'title' => __( 'Subscriber', FE_TD )
				),
			),

			'jsvars' => array(
				'frontend-content-tools-addons' => array(
					'elements' => array(),
					'wrappers' => array(
						array(
							'classes' => 'fe_h1',
							'style'   => ''
						),
						array(
							'classes' => 'fe_h2',
							'style'   => ''
						)
					)
				),
				'frontend-content-tools-config' => array(
					'block_classes' => array(

						array(
							'name'  => __( 'Clearer', FE_TD ),
							'class' => 'uk-clearer'
						),
						array(
							'name'  => '1/2 ' . __( 'Width', FE_TD ),
							'class' => 'uk-width-1-2'
						),
						array(
							'name'  => '1/3 ' . __( 'Width', FE_TD ),
							'class' => 'uk-width-1-3'
						),
						array(
							'name'  => '1/4 ' . __( 'Width', FE_TD ),
							'class' => 'uk-width-1-4'
						),
						array(
							'name'  => __( 'Auto width', FE_TD ),
							'class' => 'uk-width-auto'
						),

						array(
							'name'  => __( 'Pull Right', FE_TD ),
							'class' => 'uk-float-right'
						),

						array(
							'name'  => __( 'Alert', FE_TD ),
							'class' => 'uk-alert'
						),
						array(
							'name'  => __( 'Green', FE_TD ) . ' ' . __( 'alert modifier', FE_TD ),
							'class' => 'uk-alert-success'
						),
						array(
							'name'  => __( 'Red', FE_TD ) . ' ' . __( 'alert modifier', FE_TD ),
							'class' => 'uk-alert-danger'
						),

						array(
							'name'  => __( 'Spacing', FE_TD) . ' ' . __('top', FE_TD),
							'class' => 'fe_spacer_t'
						),

						array(
							'name'  => __( 'Spacing', FE_TD) . ' ' . __('bottom', FE_TD),
							'class' => 'fe_spacer_b'
						),

						array(
							'name'  => __( 'Spacing', FE_TD) . ' ' . __('left', FE_TD),
							'class' => 'fe_spacer_l'
						),

						array(
							'name'  => __( 'Spacing', FE_TD) . ' ' . __('right', FE_TD),
							'class' => 'fe_spacer_r'
						),


						array(
							'name'  => __( 'Border bottom', FE_TD ),
							'class' => 'fe_lined'
						),

					)
				),
			),

			'only_defaults' => array(
				'jsvars' => array(
					//'elements'      => true,
					//'wrappers'      => true,
					//'block_classes' => true
				)
			),

			'styles' => array(),

			'scripts' => array(),

		);

		$this->_from_files();

		$this->_load();

	}

	private function _from_files() {

		$elements = array(
			'<a class="uk-button uk-button-large" href="">' . __( 'Large Button', FE_TD ) . '</a>',
			'<a class="uk-button uk-button-primary uk-button-large" href="">' . __( 'Large Button', FE_TD ) . '</a>',
			'<a class="uk-button uk-button-success uk-button-large" href="">' . __( 'Large Button', FE_TD ) . '</a>',

			'<a class="uk-button" href="">' . __( 'Smaller button', FE_TD ) . '</a>',
			'<a class="uk-button uk-button-primary" href="">' . __( 'Smaller button', FE_TD ) . '</a>',
			'<a class="uk-button uk-button-success" href="">' . __( 'Smaller button', FE_TD ) . '</a>'
		);

		$icons = array(
			'facebook',
			'twitter',
			'google-plus',
			'youtube',
			'github'
		);

		foreach ( $icons as $icon ) {

			$elements[] = '<a href="" class="uk-icon-button uk-icon-' . $icon . '"></a>';

		}

		foreach ( $elements as $element ) {
			$this->config['jsvars']['frontend-content-tools-addons']['elements'][] = $element;
		}

		$elements_dir = FE_DIR . '/includes/config/elements';
		if ( file_exists( FE_DIR . '/includes/config/elements' ) ) {

			$elements_files = glob( $elements_dir . '/*.*' );

			if ( count( $elements_files ) ) {
				$eleemnts_from_files = array();
				foreach ( $elements_files as $elements_file ) {
					$this->config['jsvars']['frontend-content-tools-addons']['elements'][] = file_get_contents( $elements_file );
				}
			};

		}

	}

	private function _load() {

		$option_prefix = $this->config['option_prefix'];

		$from_db = get_option( $option_prefix . '_settings' );
		if ( $from_db ) {
			foreach ( $from_db as $setting_key => $setting_val ) {

				switch ( $setting_key ) {

					case 'roles':

						foreach ( $this->config['roles'] as $role_key => $role_val ) {

							unset( $this->config['roles'][ $role_key ]['can'] );

						}

						foreach ( $setting_val as $role_key => $role_val ) {

							if ( isset( $role_val['can'] ) ) {

								$this->config['roles'][ $role_key ]['can'] = $role_val['can'];

							}

						}

						break;

					case 'jsvars':

						$jsvar_keymap = array(
							'elements'      => 'frontend-content-tools-addons',
							'wrappers'      => 'frontend-content-tools-addons',
							'block_classes' => 'frontend-content-tools-config',
						);

						foreach ( $setting_val as $var_key => $var_val ) {

							if ( isset( $this->config['only_defaults']['jsvars'][ $var_key ] ) ) {
								continue;
							}

							if ( $var_key == 'elements' ) {
								foreach ( $var_val as $v_k => $v_v ) {
									$var_val[ $v_k ] = preg_replace( '/\\\\/', '', $v_v );
								}
							}

							$this->config['jsvars'][ $jsvar_keymap[ $var_key ] ][ $var_key ] = $var_val;

						}

						break;

				}

			}
		}

	}

}
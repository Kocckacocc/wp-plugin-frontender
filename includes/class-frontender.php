<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

class Frontender {

	private static $_instance = null;

	private $config = array();

	public function __construct() {

		$this->_constants();

		add_action( 'plugins_loaded', array( $this, 'init' ), 0 );

	}

	private function _constants() {

		define( 'FE_TD', 'frontender' );
		define( 'FE_DIR', untrailingslashit( plugin_dir_path( dirname( __FILE__ ) ) ) );
		define( 'FE_URL', untrailingslashit( plugin_dir_url( dirname( __FILE__ ) ) ) );

	}

	public static function instance() {

		if ( is_null( self::$_instance ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}

	public function init() {

		$this->_load_textdomain();

		$config = new Frontender_Config();

		$this->config = $config->config;

		if ( ! $this->_check_wp() ) {
			return false;
		}

		register_activation_hook( FE_DIR . '/frontender.php', array( $this, 'activate' ) );

		if ( is_admin() ) {
			new Frontender_Admin( $this->config );
		} else {
			new Frontender_Frontend( $this->config );
		}

	}

	private function _load_textdomain() {

		load_textdomain( FE_TD, FE_DIR . '/languages/' . get_locale() . '.mo' );

	}

	private function _check_wp() {
		global $wp_version;

		if ( version_compare( $wp_version, $this->config['wp_version'], '<' ) ) {

			add_action( 'admin_notices', function () {

				$class   = 'notice notice-error is-dismissible';
				$message = sprintf( __( 'Frontender need WP version %s or higher.', FE_TD ), $this->config->config['wp_version'] );
				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );

			} );

			return false;

		}

		return true;
	}

	public function activate() {

		delete_option( $this->config['option_prefix'] . '_settings' );

	}

	public function __clone() {
		_doing_it_wrong( __FUNCTION__, '...', null );
	}

	public function __wakeup() {
		_doing_it_wrong( __FUNCTION__, '...', null );
	}

}
<?php

$js_vars = array(
	'ajax_url' => admin_url( 'admin-ajax.php' ),

	'localization' => array(
		'settingssaved' => __( 'Settings saved!', FE_TD ),
		'You can\'t delete the last element!' =>  __( 'You can\'t delete the last element!', FE_TD ),
		'Sorry, max. 12 element allowed at the moment.' =>  __( 'Sorry, max. 12 element allowed at the moment.', FE_TD ),
		'Are you sure you delete this element?' =>  __( 'Are you sure you delete this element?', FE_TD )
	),
);


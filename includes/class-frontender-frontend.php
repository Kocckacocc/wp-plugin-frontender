<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

class Frontender_Frontend {

	public $config = array();

	public function __construct( $config ) {

		$this->config = $config;

		add_action( 'parse_query', array( $this, 'init' ) );

	}

	public function init() {

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_theme_specific' ), 99 );

		add_action( 'the_content', array( $this, 'wrap_content' ) );

		if ( ! $this->_enabled() ) {
			return;
		}

		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_assets' ) );

		add_action( 'wp_footer', array( $this, 'maybe_lockdown' ) );

		remove_filter( 'the_content', 'wpautop' );

	}

	private function _enabled() {

		return ( is_page() && $this->_can( 'pages' ) ) || ( is_single() && $this->_can( 'posts' ) );

	}

	private function _can( $action ) {
		$current_user = wp_get_current_user();

		if (
			! isset( $current_user->roles[0] ) ||
			! isset( $this->config['roles'][ $current_user->roles[0] ] ) ||
			! isset( $this->config['roles'][ $current_user->roles[0] ]['can'] )
		)
			return false;

		return in_array( $action, explode( ',', ( $this->config['roles'][ $current_user->roles[0] ]['can'] ) ) );
	}

	public function enqueue_assets() {

		wp_enqueue_media();

		$scripts = array(

			'content-tools' => array(
				'vendor/content-tools/custom/content-tools.min.js',
			),

			'frontend-content-tools-config' => array(
				'frontend/js/frontend-content-tools-config.js',
				array( 'content-tools' )
			),

			'frontend-content-tools-addons' => array(
				'frontend/js/frontend-content-tools-addons.js',
				array(
					'jquery',
					'media-models',
					'content-tools'
				)
			),

			'frontend-main' => array(
				'enqueue' => true,
				'frontend/js/frontend-main.js',
				array(
					'frontend-content-tools-config',
					'frontend-content-tools-addons'
				)
			)

		);

		if ( isset( $this->config['scripts']['frontend'] ) ) {
			$scripts = array_merge( $scripts, $this->config['scripts']['frontend'] );
		}

		foreach ( $scripts as $script_id => $script ) {

			wp_register_script( $script_id, FE_URL . '/assets/' . $script[0], isset( $script[1] ) ? $script[1] : null, null, true );

			$config_file = FE_DIR . '/includes/config/jsvars/frontend/' . $script_id . '.php';

			$js_vars = array();

			if ( file_exists( $config_file ) ) {
				require_once( $config_file );
			}

			if ( isset( $this->config['jsvars'][ $script_id ] ) ) {
				$js_vars = array_merge( $js_vars, $this->config['jsvars'][ $script_id ] );
			}

			if ( count( $js_vars ) ) {
				wp_localize_script( $script_id, str_replace( '-', '_', $script_id ), $js_vars );
			}

			if ( isset( $script['enqueue'] ) ) {
				wp_enqueue_script( $script_id );
			}

		}

	}

	public function enqueue_theme_specific() {

		$styles = array(
			'content-tools'        => 'vendor/content-tools/custom/content-tools.min.css',
			'content-tools-addons' => 'frontend/css/content-tools-addons.css',

			'frontender-main' => 'frontend/css/frontend-main.css',
		);

		if ( isset( $this->config['styles']['frontend'] ) ) {
			$styles = array_merge( $styles, $this->config['styles']['frontend'] );
		}

		foreach ( $styles as $style_id => $style_path ) {
			wp_enqueue_style( $style_id, FE_URL . '/assets/' . $style_path, null, null );
		}

		$theme_dirname = explode( '/', get_template_directory() );
		$theme_dirname = end( $theme_dirname );

		foreach ( glob( FE_DIR . '/assets/frontend/css/theme-specific/' . $theme_dirname . '/*.css' ) as $style_id => $style_path ) {
			wp_enqueue_style( 'fronender-theme-specific-' . $style_id, str_replace( FE_DIR, FE_URL, $style_path ), null, null );
		}

	}

	public function wrap_content( $content ) {
		$content = '<div data-editable data-name="content-' . $GLOBALS['post']->ID . '">' . $content . '</div>';

		return $content;
	}

	public function maybe_lockdown() {

		$post = $GLOBALS['post'];

		if ( ! is_object( $post ) )
			return;

		if ( $user_id = $this->_check_lock( $post->ID ) )
			$user = get_userdata( $user_id );
		else return;

		echo
			'<div id="post-lock-dialog" class="notification-dialog-wrap fe_lock_dialog fe_hidden">' .
			'<div class="notification-dialog-background"></div>' .
			'<div class="notification-dialog">' .

			'<div class="post-locked-message">' .
			'<div class="post-locked-avatar">' . get_avatar( $user->ID, 64 ) . '</div>' .
			'<p class="currently-editing wp-tab-first" tabindex="0">' .
			sprintf( __( 'This content is currently locked.', FE_TD ) . ' ' . __( 'If you take over, %s will be blocked from continuing to edit.', FE_TD ), esc_html( $user->display_name ) ) .
			'</p>' .
			'<p>' .
			'<a class="button fe_cancel_edit">' . __( 'Go back', FE_TD ) . '</a>' .
			'<a class="button button-primary wp-tab-last fe_relase_lock">' . __( 'Take over', FE_TD ) . '</a>' .
			'</p>' .
			'</div>' .

			'</div>' .
			'</div>';

	}

	private function _check_lock( $post_id ) {
		if ( ! $post = get_post( $post_id ) )
			return false;

		if ( ! $lock = get_post_meta( $post->ID, '_edit_lock', true ) )
			return false;

		$lock = explode( ':', $lock );
		$time = $lock[0];
		$user = isset( $lock[1] ) ? $lock[1] : get_post_meta( $post->ID, '_edit_last', true );

		$time_window = apply_filters( 'wp_check_post_lock_window', 150 );

		if ( $time && $time > time() - $time_window && $user != get_current_user_id() )
			return $user;

		return false;
	}

}


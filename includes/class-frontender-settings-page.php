<?php

if ( ! defined( 'ABSPATH' ) )
	exit;

class Frontender_Settings_Page {

	public $config = array();

	public function __construct( $config ) {

		$this->config = $config;

		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_assets' ), 10, 1 );

		add_action( 'admin_menu', array( $this, 'admin_menu' ) );

	}

	public function admin_menu() {

		add_menu_page(
			$this->config['shortname'] . ' ' . __( 'Settins', FE_TD ),
			$this->config['shortname'],
			'manage_options', // @see https://codex.wordpress.org/Roles_and_Capabilities
			'fronteder_settings',
			array( $this, 'render' ),
			'dashicons-edit',

			76 // @see https://developer.wordpress.org/reference/functions/add_menu_page/#menu-structure
		);

	}

	public function enqueue_assets( $hook = '' ) {

		if ( $hook != 'toplevel_page_fronteder_settings' )
			return;

		$uikit_components = array(
			'sticky',
			'notify',
			'htmleditor'
		);

		/*
		codemirror
		@see http://codemirror.net/doc/compress.html

		define('FE_CODEMIRROR_DEV', true);
		*/

		if ( defined( 'FE_CODEMIRROR_DEV' ) ) {
			$codemirror_main_slug = 'lib';
		} else {
			$codemirror_main_slug = 'bundled';
		}

		$styles = array(
			'uikit'      => 'vendor/uikit/dist/css/uikit.frontender.min.css',
			'codemirror' => 'vendor/codemirror/' . $codemirror_main_slug . '/codemirror.css'
		);

		foreach ( $uikit_components as $uikit_component ) {
			$styles[ 'uikit-' . $uikit_component ] = 'vendor/uikit/dist/css/components/' . $uikit_component . '.frontender.min.css';
		}

		$styles['admin-main'] = 'admin/css/admin-main.css';

		foreach ( $styles as $style_id => $style_path ) {
			wp_enqueue_style( $style_id, FE_URL . '/assets/' . $style_path, null, null );
		}

		$scripts = array();

		$scripts['codemirror'] = array(
			'vendor/codemirror/' . $codemirror_main_slug . '/codemirror.js'
		);

		$codemirror_component_urls = array();

		if ( defined( 'FE_CODEMIRROR_DEV' ) ) {

			$codemirror_components = array(
				'mode'  => array(
					'css.js',
					'gfm.js',
					'htmlembedded.js',
					'htmlmixed.js',
					'javascript.js',
					'markdown.js',
					'php.js',
					'sql.js',
					'xml.js',
					'xquery.js',
					'yaml.js',
				),
				'addon' => array(
					'active-line.js',
					'anyword-hint.js',
					'closebrackets.js',
					'closetag.js',
					'css-hint.js',
					'hardwrap.js',
					'html-hint.js',
					'javascript-hint.js',
					'mark-selection.js',
					'match-highlighter.js',
					'matchbrackets.js',
					'matchtags.js',
					'overlay.js',
					'placeholder.js',
					'show-hint.js',
					'sql-hint.js',
					'xml-fold.js',
					'xml-hint.js',
				)
			);

			foreach ( $codemirror_components as $cc_key => $cc_jss ) {
				foreach ( $cc_jss as $cc_js ) {
					$js_dir = $this->_glob_recursive( FE_DIR . '/assets/vendor/codemirror/' . $cc_key . '/' . $cc_js );
					if ( isset( $js_dir[0] ) ) {
						$codemirror_component_urls[] = str_replace( FE_DIR . '/assets/', '', $js_dir[0] );
					}

				}
			}

		}

		// marked

		$scripts['marked'] = array(
			'vendor/marked/lib/marked.js',
			array( 'codemirror' )
		);

		foreach ( $codemirror_component_urls as $component_url ) {

			$component_id = str_replace(
				array(
					'vendor/',
					'.js',
					'/'
				),
				array(
					'',
					'',
					'_'
				), $component_url );

			$scripts[ $component_id ] = array(
				$component_url,
				array( 'codemirror' )
			);

			$scripts['marked'][1][] = $component_id;

		}

		$scripts['uikit'] = array(
			'vendor/uikit/dist/js/uikit.min.js',
			array( 'jquery' )
		);

		$scripts['admin-main'] = array(
			'admin/js/admin-main.js',
			array( 'uikit', 'codemirror', 'marked' ),
			'enqueue' => true
		);

		foreach ( $uikit_components as $uikit_component ) {
			$scripts[ 'uikit-' . $uikit_component ] = array(
				'vendor/uikit/dist/js/components/' . $uikit_component . '.min.js',
				array( 'uikit' )
			);

			$scripts['admin-main'][1][] = 'uikit-' . $uikit_component;
		}

		foreach ( $scripts as $script_id => $script ) {

			wp_register_script( $script_id, FE_URL . '/assets/' . $script[0], isset( $script[1] ) ? $script[1] : null, null, true );

			$config_file = FE_DIR . '/includes/config/jsvars/admin/' . $script_id . '.php';

			$js_vars = array();

			if ( file_exists( $config_file ) ) {
				require_once( $config_file );
			}

			if ( count( $js_vars ) ) {
				wp_localize_script( $script_id, str_replace( '-', '_', $script_id ), $js_vars );
			}

			if ( isset( $script['enqueue'] ) ) {
				wp_enqueue_script( $script_id );
			}

		}

	}

	private function _glob_recursive( $pattern, $flags = 0 ) {
		$files = glob( $pattern, $flags );

		foreach ( glob( dirname( $pattern ) . '/*', GLOB_ONLYDIR | GLOB_NOSORT ) as $dir ) {
			$files = array_merge( $files, $this->_glob_recursive( $dir . '/' . basename( $pattern ), $flags ) );
		}

		return $files;
	}

	public function render() {

		?>
		<div class="uk-container uk-container-center uk-margin-top uk-margin-large-bottom fe_settings_screen">
			<div class="uk-grid">
				<div class="uk-width-1-1 uk-row-first">
					<div class="uk-panel uk-panel-box">
						<h2>
							<?php _e( 'Frontender Settings', FE_TD ) ?>
							<code class="uk-float-right uk-block-default"><?php echo 'v' . $this->config['version'] ?></code>
						</h2>
					</div>
				</div>
			</div>
			<div class="uk-grid">
				<div class="uk-width-medium-4-5 uk-row-first">
					<div class="uk-panel uk-panel-box uk-panel-box-secondary">
						<div class="uk-grid">
							<?php $this->_render_tabs() ?>
						</div>
					</div>
				</div>
				<div class="uk-width-medium-1-5">
					<div class="uk-sticky-placeholder">
						<div class="uk-panel uk-panel-box" data-uk-sticky="{top:35}">
							<ul class="uk-nav uk-nav-side">
								<?php $this->_render_sticky() ?>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php
	}

	private function _render_tabs() {

		$tabs = array(
			'Permissions' => __( 'Permissions', FE_TD ),
			'Elements'    => __( 'Elements', FE_TD ),
			'Styles'      => __( 'Styles', FE_TD ),

			//'Tools'       => __( 'Tools', FE_TD ),

		);

		echo
			'<div class="uk-width-medium-1-5">' .
			'<ul class="uk-tab uk-tab-left fe_tab_nav" data-uk-tab="{connect:\'#fe_tab_content\'}">';

		foreach ( $tabs as $tab_title ) {

			echo
				'<li' . ( ! isset( $not_first ) ? ' class="uk-active"' : '' ) . "><a>$tab_title</a></li>";

			$not_first = true;

		}

		echo
			'</ul>' .
			'</div>' .
			'<form class="fe_settings_data uk-width-medium-4-5">' .
			'<ul id="fe_tab_content" class="uk-switcher fe_tab_content">';

		if ( isset( $not_first ) ) {
			unset( $not_first );
		}
		foreach ( $tabs as $tab_key => $tab_title ) {

			echo
				'<li class="' . ( ! isset( $not_first ) ? 'uk-active' : '' ) . ' fe_' . strtolower( $tab_key ) . '">' .
				"<h3>$tab_title</h3>";

			$this->_render_tab( $tab_key );

			echo
			'</li>';

			$not_first = true;

		}

		echo
			'</ul>' .
			'<div class="fe_actions">' .
			'<button class="fe_save uk-button uk-button-primary" type="button">' . __( 'Save settings', FE_TD ) . '</button>' .
			'<button class="fe_reset_settings uk-button uk-button-small uk-float-right" type="button">' . __( 'Reset settings', FE_TD ) . '</button>' .
			'</div>' .
			'</form>';

	}

	private function _render_tab( $tab_key ) {

		switch ( $tab_key ) {

			case 'Permissions':

				$actions = array(
					'pages' => __( 'Edit Pages', FE_TD ),
					'posts' => __( 'Edit Posts', FE_TD ),
					'lock'  => __( 'Relase Lock', FE_TD )
				);

				echo
					'<div class="uk-overflow-container">' .
					'<table class="uk-table uk-table-condensed">' .
					'<thead>' .
					'<tr>' .
					'<th>' . __( 'Role', FE_TD ) . '</th>';

				foreach ( $actions as $action_title ) {
					echo "<th>$action_title</th>";
				}

				echo
					'</tr>' .
					'</thead>' .
					'<tbody>';

				foreach ( $this->config['roles'] as $id => $role ) {

					$can = isset( $role['can'] ) ? explode( ',', $role['can'] ) : array();

					echo
						'<tr>' .
						'<td>' . $role['title'] . '</td>';

					foreach ( $actions as $action_key => $action_title ) {

						$this_checked = in_array( $action_key, $can ) ? ' checked="checked"' : '';
						echo
							'<td><input type="checkbox" name="' . $tab_key . '_' . $id . '_' . $action_key . '" ' . $this_checked . '/></td>';

					}

					echo
					'</tr>';
				}

				echo
					'</tbody>' .
					'</table>' .
					'</div>';

				break;

			case 'Elements' :

				$elements = $this->config['jsvars']['frontend-content-tools-addons']['elements'];

				echo
				'<ul class="uk-tab fe_elements_tabs" data-uk-tab="{connect:\'#fe_elements_tabs_contents\'}">';

				foreach ( $elements as $element ) {
					echo
						'<li' . ( ! isset( $not_first ) ? ' class="uk-active"' : '' ) . '>' .
						'<a><i class="uk-icon-bookmark-o"></i></a>' .
						'</li>';

					$not_first = true;
				}

				echo
					'<li class="uk-disabled">' .
					'<a class="fe_new_element">' . __( '+ New', FE_TD ) . '</a>' .
					'</li>' .

					'</ul>' .

					'<ul id="fe_elements_tabs_contents" class="uk-switcher uk-margin">';

				$editor_config = array(
					"mode:'split'",
					"lblPreview:'" . __( 'Preview', FE_TD ) . "'",
					"height:'150'",
					"iframe:'". FE_URL . '/assets/admin/css/admin-preview.css' ."'" //todo kocckacocc: more css to preview!
				);

				$editor_config = 'data-uk-htmleditor="' . implode( ',', $editor_config ) . '"';

				if ( isset( $not_first ) ) {
					unset( $not_first );
				}
				foreach ( $elements as $element_key => $element ) {
					echo
						'<li class="fe_element_box' . ( ! isset( $not_first ) ? ' uk-active' : '' ) . '">' .

						'<textarea name="Elements_' . $element_key . '" ' . $editor_config . '>' . $element . '</textarea>' .
						'<div class="uk-margin-small-top">' .
						'<span class="uk-button uk-button-mini fe_del_element">' . __( 'Delete element', FE_TD ) . '</span>' .
						'</div>' .
						'</li>';

					$not_first = true;
				}

				echo
				'</ul>';

				break;

			case 'Styles':

				$wrappers = $this->config['jsvars']['frontend-content-tools-addons']['wrappers'];
				$classers = $this->config['jsvars']['frontend-content-tools-config']['block_classes'];

				echo
					'<ul class="uk-tab" data-uk-tab="{connect:\'#fe_styles_types\'}">' .
					'<li><a>' . __( 'Inline', FE_TD ) . '</a></li>' .
					'<li><a>' . __( 'Block', FE_TD ) . '</a></li>' .
					'</ul>' .
					'<ul id="fe_styles_types" class="uk-switcher uk-margin">' .
					'<li>' .

					'<div class="uk-overflow-container">' .
					'<table class="uk-table uk-table-condensed">' .
					'<thead>' .
					'<tr>' .
					'<th>' . __( 'Class(es)', FE_TD ) . '</th>' .
					'<th>' . __( 'Style tag', FE_TD ) . '</th>' .
					'<th></th>' .
					'</tr>' .
					'</thead>' .
					'<tbody class="fe_wrappers">';

				function render_wrapper( $wrapper_key, $wrapper ) {

					return
						'<tr>' .
						'<td>' .
						'<input type="text" class="uk-form-small uk-width-1-1" name="Wrapper_' . $wrapper_key . '_classes" value="' . $wrapper['classes'] . '"/>' .
						'</td>' .
						'<td>' .
						'<input type="text" class="uk-form-small uk-width-1-1" name="Wrapper_' . $wrapper_key . '_style" value="' . $wrapper['style'] . '"/>' .
						'</td>' .
						'<td><span class="uk-button uk-button-mini fe_del_wrapper"><i class="uk-icon-close"></i></span></td>' .
						'</tr>';

				}

				foreach ( $wrappers as $wrapper_key => $wrapper ) {

					echo
					render_wrapper( $wrapper_key, $wrapper );

				}

				$wrapper_clone = htmlentities(render_wrapper( 'random', array( 'classes' =>'', 'style' =>'' ) ));

				echo
					'</tbody>' .
					'</table>' .
					'</div>' .

					'<div class="uk-margin-top">' .
					'<span data-wrapper_clone="' . $wrapper_clone . '" class="uk-button uk-button-mini fe_new_wrapper">' . __( 'Add wrapper', FE_TD ) . '</span>' .
					'</div>' .

					'</li>' .
					'<li>' .

					'<div class="uk-overflow-container">' .
					'<table class="uk-table uk-table-condensed">' .
					'<thead>' .
					'<tr>' .
					'<th>' . __( 'Name', FE_TD ) . '</th>' .
					'<th>' . __( 'Class', FE_TD ) . '</th>' .
					'<th></th>' .
					'</tr>' .
					'</thead>' .
					'<tbody class="fe_classers">';

				function render_classer( $classer_key, $classer ) {

					return
						'<tr>' .
						'<td>' .
						'<input type="text" class="uk-form-small uk-width-1-1" name="Blockclass_' . $classer_key . '_name" value="' . $classer['name'] . '"/>' .
						'</td>' .
						'<td>' .
						'<input type="text" class="uk-form-small uk-width-1-1" name="Blockclass_' . $classer_key . '_class" value="' . $classer['class'] . '"/>' .
						'</td>' .
						'<td><span class="uk-button uk-button-mini fe_del_classer"><i class="uk-icon-close"></i></span></td>' .
						'</tr>';

				}

				foreach ( $classers as $classer_key => $classer ) {

					echo
					render_classer( $classer_key, $classer );

				}

				$classer_clone = htmlentities(render_classer( 'random',array( 'name' =>'', 'class' =>'' ) ));

				echo
					'</tbody>' .
					'</table>' .
					'</div>' .

					'<div class="uk-margin-top">' .
					'<span data-classer_clone="' . $classer_clone . '" class="uk-button uk-button-mini fe_new_classer">' . __( 'Add block class', FE_TD ) . '</span>' .
					'</div>' .


					'</li>' .
					'</ul>';

				break;

			default;

				echo
					'<pre>' . __( 'Coming soon!...', FE_TD ) . '</pre>';

		}

	}

	private function _render_sticky() {

		foreach ( $this->config['admin']['sticky'] as $id => $item ) {

			$class = isset( $item['class'] ) ? ' class="' . $item['class'] . '"' : '';
			$title = isset( $item['title'] ) ? $item['title'] : '';
			$href  = '';
			$modal = '';

			if ( isset( $item['link'] ) ) {

				$href = ' href="' . $item['link'] . '" target="_blank"';

			} elseif ( isset( $item['modal'] ) ) {

				$href  = ' href="#fe_modal_' . $id . '" data-uk-modal';
				$modal =
					'<div id="fe_modal_' . $id . '" class="uk-modal">' .
					'<div class="uk-modal-dialog">' .
					'<a class="uk-modal-close uk-close"></a>' .
					'<h2>' . $item['title'] . '</h2>' .
					'<p>' . $item['modal'] . '</p>' .
					'</div>' .
					'</div>';

			}

			if ( $href != '' ) {
				$title = "<a$href>$title</a>";
			}

			echo
			"<li$class>$title$modal</li>";

		}

	}

}